﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

[System.Serializable]
public class Board
{
    private const int MIN_SIZE = 4;
    private const int MAX_SIZE = 6;

    [Tooltip("The value power by 2 will be the size of the board")]
    [SerializeField] [Range(MIN_SIZE, MAX_SIZE)] private int boardSqrRootedSize = 4;
    
    [Tooltip("If true, the \"boardSqrRottedSize\" will be ignored")]
    [SerializeField] private bool randomSizeBoard = false;

    [SerializeField] private GameObject tilePrefab = null;

    [SerializeField] private Cell[,] grid;

    [SerializeField] private ScriptableSettings settings = null;

    public Cell[,] Grid { get => grid; }
    public int BoardSqrRootedSize { get => boardSqrRootedSize; }

    public void Create(Transform parent)
    {
        if (randomSizeBoard)
        {
            boardSqrRootedSize = Random.Range(MIN_SIZE, MAX_SIZE);
        }

        grid = new Cell[BoardSqrRootedSize, BoardSqrRootedSize];
        for (int i = 0; i < BoardSqrRootedSize; i++)
        {
            for (int j = 0; j < BoardSqrRootedSize; j++)
            {
                Vector3Int pos = new Vector3Int(i, 0, j);
                GameObject cellGO = MonoBehaviour.Instantiate<GameObject>(tilePrefab, pos, Quaternion.identity, parent);
                cellGO.transform.localScale = Vector3.zero;
                Cell cell = new Cell(cellGO.transform);
                grid[i, j] = cell;
            }
        }
    }

    public IEnumerator Show()
    {
        WaitForSeconds waiting = new WaitForSeconds(0.025f);
        for (int i = 0; i < BoardSqrRootedSize; i++)
        {
            for (int j = 0; j < BoardSqrRootedSize; j++)
            {
                yield return waiting;
                grid[i,j].Transform.DOScale(Vector3.one, settings.AnimationTime.Medium)
                    .SetEase(Ease.OutBounce);
            }
        }
    }

    public void FillBoard(PoolManager pool, ref Vector3 player1position, ref Vector3 player2position, bool playersArePlaced = false)
    {
        int player1Pos = Random.Range(0, Size / 2);
        int player2Pos = Random.Range(Size / 2, Size);

        for (int i = 0; i < boardSqrRootedSize; i++)
        {
            for (int j = 0; j < boardSqrRootedSize; j++)
            {
                Cell cell = grid[i, j];
                if (cell.Placeble != null) continue;

                int currentPlace = i * boardSqrRootedSize + j;

                if (currentPlace == player1Pos)
                {
                    player1position = cell.Transform.position;
                    continue;
                }

                if (currentPlace == player2Pos)
                {
                    player2position = cell.Transform.position;
                    continue;
                }

                cell.Placeble = pool.Spawn<Collectable>(Vector3.zero, Quaternion.identity, Vector3.zero);
            }
        }
    }

    public int Size
    {
        get { return BoardSqrRootedSize * BoardSqrRootedSize;  }
    }


}
