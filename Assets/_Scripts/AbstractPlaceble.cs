﻿using UnityEngine;
using DG.Tweening;
public abstract class AbstractPlaceble : MonoBehaviour
{
    [SerializeField] private float yOffset = 0f;
    [SerializeField] private ScriptableSettings settings = null;

    protected virtual void OnEnable()
    {
        Invoke("Show", 2f);
    }

    private void Show()
    {
        transform.DOScale(Vector3.one, settings.AnimationTime.Long)
            .SetEase(Ease.OutBounce);
    }

    protected virtual void OnDisable()
    {
        transform.localScale = Vector3.zero;
    }

    public virtual void Place(Vector3 position)
    {
        transform.position = position + Vector3.up * yOffset;
    }
}
