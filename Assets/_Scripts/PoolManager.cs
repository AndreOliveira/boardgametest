﻿using UnityEngine;

public class PoolManager
{
    private GameObject prefab = null;
    private int amount = 10;
    private GameObject[] pool;

    private GameObject[] Pool { get => pool; }

    public PoolManager(GameObject prefab, int size, Transform parent)
    {
        this.prefab = prefab;
        amount = size;
        Populate(parent);
    }

    private void Populate(Transform parent)
    {
        pool = new GameObject[amount];

        for (int i = 0; i < amount; i++)
        {
            GameObject pooled = MonoBehaviour.Instantiate<GameObject>(prefab, parent);
            pooled.SetActive(false);
            pool[i] = pooled;
        }
    }

    public GameObject Spawn(Vector3 pos, Quaternion rot, Vector3 scale)
    {
        if (pool == null) return null;

        for (int i = 0; i < pool.Length; i++)
        {
            if (!pool[i].activeInHierarchy)
            {
                pool[i].transform.position = pos;
                pool[i].transform.rotation = rot;
                pool[i].transform.localScale = scale;
                pool[i].SetActive(true);
                return pool[i];
            }
        }
        return null;
    }

    public GameObject Spawn()
    {
        return Spawn(Vector3.zero, Quaternion.identity, Vector3.one);
    }

    public GameObject Spawn(Vector3 pos)
    {
        return Spawn(pos, Quaternion.identity, Vector3.one);
    }

    public T Spawn<T>()
    {
        return Spawn().GetComponent<T>();
    }

    public T Spawn<T>(Vector3 pos)
    {
        return Spawn(pos, Quaternion.identity, Vector3.one).GetComponent<T>();
    }

    public T Spawn<T>(Vector3 pos, Quaternion rot, Vector3 scale)
    {
        return Spawn(pos, rot, scale).GetComponent<T>();
    }
}
