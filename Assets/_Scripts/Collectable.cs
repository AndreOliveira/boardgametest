﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : AbstractPlaceble
{
    [SerializeField] private CollectableType type = CollectableType.TURN;

    public CollectableType Type { get => type; set => type = value; }
}

public enum CollectableType
{
    TURN = 0,
    ATTACK,
    HEALTH,
    DICE
}