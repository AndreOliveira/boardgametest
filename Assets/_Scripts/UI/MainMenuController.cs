﻿using UnityEngine;
using DG.Tweening;

public class MainMenuController : MonoBehaviour
{
    [SerializeField] private CanvasGroup currentMenu = null;
    [SerializeField] private CanvasGroup[] allMenus = null;

    [SerializeField] private ScriptableSettings settings = null;

    private void Start()
    {
        for (int i = 0; i < allMenus.Length; i++)
        {
            allMenus[i].alpha = 0f;
            allMenus[i].GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        }

        currentMenu?.DOFade(1f, settings.AnimationTime.Short)
            .SetEase(Ease.InOutBack);
    }

    /// <summary>
    /// Changes menu smoothly, fading out the current and fading in the new
    /// </summary>
    /// <param name="to">the menu what will be showed</param>
    public void ChangeMenu (CanvasGroup to)
    {
        if (currentMenu == null)
        {
            to.DOFade(1f, settings.AnimationTime.Short)
                .SetEase(Ease.InOutQuad);
                currentMenu = to;

            return;
        }

        currentMenu.DOFade(0f, settings.AnimationTime.Short)
            .SetEase(Ease.InOutQuad)
            .OnComplete(() =>
            {
                to.DOFade(1f, settings.AnimationTime.Short)
                .SetEase(Ease.InOutQuad);
                currentMenu = to;
            });
    }

}
