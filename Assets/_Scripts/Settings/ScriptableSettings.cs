﻿using UnityEngine;

[CreateAssetMenu(fileName = "Settings", menuName = "ScriptableSettings", order = 0)]
public class ScriptableSettings : ScriptableObject
{
    [SerializeField] private AnimationTime animationTime;

    public AnimationTime AnimationTime { get => animationTime; }
}

[System.Serializable]
public class AnimationTime
{
    [SerializeField] private float @short = 0.25f;
    [SerializeField] private float medium = 0.5f;
    [SerializeField] private float @long = 1f;

    public float Short { get => @short; set => @short = value; }
    public float Medium { get => medium; set => medium = value; }
    public float Long { get => @long; set => @long = value; }
}