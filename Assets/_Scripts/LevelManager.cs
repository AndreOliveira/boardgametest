﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.UIElements;

public class LevelManager : MonoBehaviour
{
    [Header("Board")]
    [SerializeField] private Transform boardParent = null;
    [SerializeField] private Board board = new Board();
    
    [Header("Placebles")]
    [SerializeField] private ScriptableCharacter player1 = null;
    [SerializeField] private ScriptableCharacter player2 = null;
    [SerializeField] private Transform collectablesParent = null;
    [SerializeField] private GameObject collectablePrefab = null;

    [Header("Audio")]
    [SerializeField] private AudioManager audioManager = null;

    private Character char1;
    private Character char2;
    private PoolManager pool;
    private bool finishedBattle;

    void Start()
    {
        //player positions (will be updated with fill board method)
        Vector3 p1pos = Vector3.zero;
        Vector3 p2pos = Vector3.zero;

        board.Create(boardParent);
        StartCoroutine(board.Show());
        pool = new PoolManager(collectablePrefab, board.Size, collectablesParent);
        board.FillBoard(pool, ref p1pos, ref p2pos);

        char1 = Instantiate<GameObject>(player1.Prefab, transform).GetComponent<Character>();
        char1.transform.localScale = Vector3.zero;
        char1.HasPlayed = false;
        char1.Place(p1pos);

        char2 = Instantiate<GameObject>(player2.Prefab, transform).GetComponent<Character>();
        char2.transform.localScale = Vector3.zero;
        char2.HasPlayed = false;
        char2.Place(p2pos);

        StartCoroutine(GameLoop());
    }

    private IEnumerator GameLoop()
    {
        yield return new WaitForSeconds(3f);
        finishedBattle = true;

        while (char1.IsAlive && char2.IsAlive)
        {
            SetupTurn();
            yield return new WaitForSeconds(1f);

            char1.Move();
            yield return new WaitUntil(() => char1.HasPlayed);

            if (HasBattle())
            {
                StartBattle(char1, char2);
            }
            yield return new WaitUntil(() => finishedBattle);
            if (!char1.IsAlive || !char2.IsAlive) break;


            char2.Move();
            yield return new WaitUntil(() => char2.HasPlayed);

            if (HasBattle())
            {
                StartBattle(char2, char1);
            }
            yield return new WaitUntil(() => finishedBattle);
        }

        if (char1.IsAlive)
        {
            GameOver(char1, char2);
            yield break;
        }

        GameOver(char2, char1);
    }

    private void SetupTurn()
    {
        char1.ScrCharacter.MovesLeft = char1.ScrCharacter.InitialMoves;
        char1.HasPlayed = false;

        char2.ScrCharacter.MovesLeft = char2.ScrCharacter.InitialMoves;
        char2.HasPlayed = false;
    }

    private void StartBattle(Character attacker, Character defender)
    {
        audioManager.ChangeMusic(AudioManager.MusicClip.BATTLE);
        finishedBattle = false;


        finishedBattle = true;
        audioManager.ChangeMusic(AudioManager.MusicClip.MAIN);
    }

    private bool HasBattle()
    {
        for (int i = 0; i < board.BoardSqrRootedSize; i++)
        {
            for (int j = 0; j < board.BoardSqrRootedSize; j++)
            {
                if (board.Grid[i,j].Placeble == char1)
                {
                    bool firstRow = i == 0;
                    bool firstColumn = j == 0;

                    bool lastRow = i == board.BoardSqrRootedSize - 1;
                    bool lastColumn = j == board.BoardSqrRootedSize - 1;

                    //check left
                    if (!firstColumn)
                    {
                        if(board.Grid[i - 1, j].Placeble == char2)
                        {
                            return true;
                        }
                    }
                    //check right
                    if (!lastColumn)
                    {
                        if (board.Grid[i + 1, j].Placeble == char2)
                        {
                            return true;
                        }
                    }
                    //check up
                    if (!lastRow)
                    {
                        if (board.Grid[i, j+1].Placeble == char2)
                        {
                            return true;
                        }
                    }
                    //check down
                    if (!firstRow)
                    {
                        if (board.Grid[i,j-1].Placeble == char2)
                        {
                            return true;
                        }
                    }

                    break;
                }
            }
        }

        return false;
    }

    private void GameOver(Character winner, Character loser)
    {
        throw new System.Exception("method not implemented");
    }

}