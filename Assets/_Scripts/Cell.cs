﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Cell
{
    private Transform transform;
    private AbstractPlaceble placeble;

    public Cell(Transform transform)
    {
        this.transform = transform;
    }

    public AbstractPlaceble Placeble
    {
        get => placeble;
        set
        {
            placeble = value;
            placeble.Place(transform.position);
        }
    }
    public Transform Transform { get => transform; }
}
