﻿using UnityEngine;

[CreateAssetMenu(fileName = "Character", menuName = "Gameplay/Character", order = 0)]
public class ScriptableCharacter : ScriptableObject
{
    [SerializeField] private string charName;
    [SerializeField] private GameObject prefab;
    [SerializeField] private int hp = 0;
    [SerializeField] private int maxHP = 0;
    [SerializeField] private int attack = 0;
    [SerializeField] private int movesLeft = 3;
    [SerializeField] private int initialMoves = 3;

    public delegate void OnIntChange(int newValue);
    public OnIntChange onHpChange;
    public OnIntChange onMovesChange;

    public string CharName { get => charName; set => charName = value; }
    public int Attack { get => attack; set => attack = value; }
    public int InitialMoves { get => initialMoves; }
    public GameObject Prefab { get => prefab; }
    public int MovesLeft
    {
        get => movesLeft;
        set
        {
            movesLeft = value;
            onMovesChange?.Invoke(movesLeft);
        }
    }
    public int Hp
    {
        get => hp;
        set
        {
            hp = Mathf.Clamp(value, 0, maxHP);
            onHpChange?.Invoke(hp);
        }
    }

    public int MaxHP { get => maxHP; }
}
