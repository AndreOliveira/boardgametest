﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : AbstractPlaceble
{
    [SerializeField] private ScriptableCharacter scrCharacter = null;
    [SerializeField] private bool hasPlayed = false;

    public ScriptableCharacter ScrCharacter { get => scrCharacter; set => scrCharacter = value; }

    public bool IsAlive
    {
        get => scrCharacter.Hp > 0;
    }

    public bool HasPlayed { get => hasPlayed; set => hasPlayed = value; }

    public void Move()
    {
        throw new System.Exception("method not implemented");
    }
}
