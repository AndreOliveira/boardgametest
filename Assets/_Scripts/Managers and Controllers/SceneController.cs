﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class SceneController : MonoBehaviour
{
    [SerializeField] private Color fadeColor = Color.white;
    [SerializeField] private ScriptableSettings settings;

    private Image fadeScreen;
    private AsyncOperation asyncOp;

    public void Start()
    {
        fadeScreen = GetComponent<Image>();
        fadeScreen.color = fadeColor;
        fadeScreen.DOFade(0f, settings.AnimationTime.Medium)
            .SetEase(Ease.InOutQuad);

        transform.SetAsLastSibling();
    }

    #region scene handle
    public void LoadSceneAsync(string scene)
    {
        asyncOp = SceneManager.LoadSceneAsync(scene);
        asyncOp.allowSceneActivation = false;

        fadeScreen.DOFade(1f, settings.AnimationTime.Medium)
            .SetEase(Ease.InOutQuad)
            .OnComplete(AllowChangeScene);
    }

    public void RestartScene()
    {
        LoadSceneAsync(SceneManager.GetActiveScene().name);
    }

    private void AllowChangeScene()
    {
        asyncOp.allowSceneActivation = true;
    }
    #endregion

    public void QuitGame()
    {
        fadeScreen.DOFade(1f, settings.AnimationTime.Medium)
            .SetEase(Ease.InOutQuad)
            .OnComplete(
                () =>
                {
                    Application.Quit();
                }
            );
    }
}