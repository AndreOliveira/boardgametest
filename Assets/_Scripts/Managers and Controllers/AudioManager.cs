﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using DG.Tweening;

public class AudioManager : MonoBehaviour
{
    private const string MASTER_VOLUME_KEY = "MasterVolume";
    private const string MUSIC_KEY = "Music";
    private const string SFX_KEY = "SFX";
    private const float MIN_VOLUME = 0.0001f;
    private const float MAX_VOLUME = 1f;

    private MusicClip currentMusic;

    [Header("Settings")]
    [SerializeField] private AudioMixer mixer = null;
    [SerializeField] private ScriptableSettings settings = null;
    [SerializeField] private AudioSource musicSource = null;
    [SerializeField] private AudioSource sfxSource = null;

    [Header("AudioClips")]
    [SerializeField] private AudioClip mainMusic = null;
    [SerializeField] private AudioClip battleMusic = null;

    [Header("HUD")]
    [SerializeField] private Slider musicSlider = null;
    [SerializeField] private Slider sfxSlider = null;

    void Start()
    {
        //setup music
        float musicVol = PlayerPrefs.HasKey(MUSIC_KEY) ? PlayerPrefs.GetFloat(MUSIC_KEY) : 1f;
        if (musicSlider) musicSlider.value = musicVol;
        SetVolume(musicVol, MUSIC_KEY, false);

        //setup sfx
        float sfxVol = PlayerPrefs.HasKey(SFX_KEY) ? PlayerPrefs.GetFloat(SFX_KEY) : 1f;
        if (sfxSlider) sfxSlider.value = sfxVol;
        SetVolume(sfxVol, SFX_KEY, false);

        MasterAudioFade(true);

        musicSource.Play();
    }

    /// <summary>
    /// to fade the audio in or out, useful to change scenes
    /// </summary>
    /// <param name="fadeIn"></param>
    public void MasterAudioFade(bool fadeIn)
    {
        float startVolume = fadeIn ? GetDecibelsFrom(MIN_VOLUME) : GetDecibelsFrom(1f);
        float endVolume = fadeIn ? GetDecibelsFrom(1f) : GetDecibelsFrom(MIN_VOLUME);

        mixer.SetFloat(MASTER_VOLUME_KEY, startVolume);
        mixer.DOSetFloat(MASTER_VOLUME_KEY, endVolume, settings.AnimationTime.Medium)
            .SetEase(Ease.InOutQuad);
    }
    
    /// <summary>
    /// set volume to audio mixer and may update PlayerPrefs
    /// </summary>
    /// <param name="volume"> new volume what is clamped between 0.0001f and 1f</param>
    /// <param name="parameterKey">it is to specified if will set Music or SFX</param>
    /// <param name="updatePlayerPrefs">Optional parameter what is by default true, but Do you want to update volume playerprefs?</param>
    private void SetVolume(float volume, string parameterKey, bool updatePlayerPrefs = true)
    {
        float decebels = GetDecibelsFrom(volume);

        mixer.SetFloat(parameterKey, decebels);

        if (updatePlayerPrefs)
        {
            PlayerPrefs.SetFloat(parameterKey, volume);
        }
    }

    /// <summary>
    /// When music in UI is updated by the player, the music settings are updated
    /// </summary>
    public void OnUpdateMusicVolume()
    {
        float music = musicSlider.value;
        SetVolume(music, MUSIC_KEY);
    }

    /// <summary>
    /// When SFX in UI is updated by the player, the sfx settings are updated
    /// </summary>
    public void OnUpdateSFXVolume()
    {
        float sfx = sfxSlider.value;
        SetVolume(sfx, SFX_KEY);
    }

    /// <summary>
    /// Save volume settings to playerprefs
    /// </summary>
    public void Save()
    {
        PlayerPrefs.Save();
    }

    /// <summary>
    /// convert float value to decibels
    /// </summary>
    /// <param name="value"> audio volume </param>
    /// <returns></returns>
    private float GetDecibelsFrom(float value)
    {
        float clampedValue = Mathf.Clamp(value, MIN_VOLUME, MAX_VOLUME);
        return Mathf.Log10(clampedValue) * 20f;
    }

    /// <summary>
    /// smoothly change game music
    /// </summary>
    /// <param name="nextMusic"></param>
    public void ChangeMusic(MusicClip nextMusic)
    {
        if (currentMusic == nextMusic) return;

        //setup next clip
        AudioClip clip;
        switch (nextMusic)
        {
            case MusicClip.BATTLE:
                clip = battleMusic;
                break;
            default:
                clip = mainMusic;
                break;
        }

        //fade in/out audio to change music smoothly
        mixer.DOSetFloat(MASTER_VOLUME_KEY, 0f, settings.AnimationTime.Medium)
            .SetEase(Ease.InOutQuad)
            .OnComplete(() =>
            {
                musicSource.clip = clip;
                musicSource.Play();
                mixer.DOSetFloat(MASTER_VOLUME_KEY, 1f, settings.AnimationTime.Medium)
                    .SetEase(Ease.InOutQuad);
            });

    }

    public void PlaySFX(AudioClip clip)
    {
        sfxSource.PlayOneShot(clip);
    }

    public enum MusicClip
    {
        MAIN,
        BATTLE
    }
}